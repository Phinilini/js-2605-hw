// Слайдер
// Створіть слайдер кожні 3 сек змінюватиме зображення
// Зображення для відображення
// https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
// https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
// https://naukatv.ru/upload/files/shutterstock_418733752.jpg
// https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
// https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg


window.addEventListener("load", () => {
    let display = document.querySelector("img.slider-display");
    const imageList = [
        "https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
        "https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
        "https://th-thumbnailer.cdn-si-edu.com/TjED0cbsP4BoIKYXcJ-TDlGCUIk=/1000x750/filters:no_upscale()/https://tf-cmsv2-smithsonianmag-media.s3.amazonaws.com/filer/6c/74/6c747034-a628-4aca-b56d-a341ff0d10ee/jaxa_venus.png",
        "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
        "https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg "
    ]

    let intervalValue = 3000, index = 0,
        intervalHandler = setInterval(changeImage, intervalValue);
    
    if (display.getAttribute("src") == "") {
        changeImage();
    }

    function changeImage() {
        display.setAttribute("src", `${imageList[index % imageList.length]}`);
        index = index + 1 % imageList.length;
    }
}, false)