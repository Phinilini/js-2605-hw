// Створіть програму секундомір.
// * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
// * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС
// * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції

window.onload = () => {
    const get = s => document.querySelector(s);

    let container = get("div.container-stopwatch");
    let display = get("div.stopwatch-display");
    let buttons = get("div.stopwatch-control");

    let startBtn = buttons.children[0];
    let stopBtn = buttons.children[1];
    let resetBtn = buttons.children[2];

    let time = 0, intervalHandler;
    const colors = [
        "green",
        "red",
        "silver"
    ];

    startBtn.onclick = () => {
        if (intervalHandler !== undefined)
            return;
        intervalHandler = setInterval(passTime, 10);
        changeColor(display, "green");
    }

    stopBtn.onclick = () => {
        intervalHandler = clearInterval(intervalHandler);
        changeColor(display, "red");
    }

    resetBtn.onclick = () => {
        intervalHandler = clearInterval(intervalHandler);
        time = 0;
        setTime(time);
        changeColor(display, "silver");
    }

    function changeColor(el, color) {
        colors.forEach(item => {
            [...el.classList].forEach((cl => {
                if (cl === item) {
                    el.classList.remove(cl);
                }
            }))
        });
        el.classList.add(color);
    }

    // 00:00:00 ;; mm:ss:tt
    // m = s * 60
    // s = t * 100
    // t = 10ms
    function passTime() {
        setTime(time++);
    }

    function setTime(time) {
        display.children[0].innerText = Math.floor(time / 100 / 60);
        time %= 6000
        display.children[1].innerText = Math.floor(time / 100);
        display.children[2].innerText = time % 100;
        [...display.children].forEach(el => {
            if (el.innerText.length < 2)
                el.innerText = "0" + el.innerText;
        });
    }
}