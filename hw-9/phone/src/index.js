// Реалізуйте програму перевірки телефону
// * Використовуючи JS Створіть поле для введення телефону та кнопку збереження
// * Користувач повинен ввести номер телефону у форматі 000-000-00-00
// * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.

window.onload = () => {
    let body = document.querySelector("body");
    let input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "000-000-00-00");
    let btn = document.createElement("input");
    btn.setAttribute("type", "button");
    btn.setAttribute("value", "Зберегти");

    body.append(input);
    body.append(btn);

    let pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
    
    btn.onclick = () => {
        let rm = document.querySelector("div");
        if (rm)
            rm.remove();

        if (pattern.exec(input.value)) {
            body.classList.add("green");
            document.location.href = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg"
        } else {
            let div = document.createElement("div");
            div.innerText = new Error("Error: write a valid phone number");
            body.prepend(div);
        }
    }
}