// Вывести каждого героя отдельной карточкой с указанием. Имени, половой принадлежности, рост, цвет кожи, 
// год рождения и планету на которой родился.

const url = "https://swapi.dev/api/people";

class Character {
    constructor(name, gender, height, skin_color, birth_year, homeworld) {
        this.name = name;
        this.gender = gender;
        this.height = height;
        this.skin_color = skin_color;
        this.birth_year = birth_year;

        // Так як планета надходить як посилання, треба створити запит
        this.p = fetch(homeworld)
            .then(response => response.json())
            .then(data => {
                this.homeworld = data.name;
            })
    }

    getHTML() {
        // Створюємо картки коли прийде відповідь від сервера
        // Щоб це працювало, скористаємось власним промісом
        this.p.finally(() => {
            const res = document.createElement('div');
            res.classList.add("card-wrapper");

            for (let prop in this) {
                if (prop === 'p')
                    continue;

                const entry = document.createElement('div');
                entry.classList.add("card-entry");

                // Створюємо спани для кожного параметра персонажа
                const key = document.createElement('span');
                key.classList.add('card-prop');
                key.innerText = prop;

                const value = document.createElement('span');
                value.classList.add('card-value');
                value.innerText = this[prop];

                entry.append(key, value);

                // entry.innerText = `${prop}: ${this[prop]}`;

                res.append(entry);
            }

            const btn = document.createElement('button');
            btn.classList.add('card-btn');
            btn.innerText = "Save";

            btn.addEventListener('click', saveCharacter);

            res.append(btn);

            document.querySelector('.wrapper').append(res);
        })
    }
}

function saveCharacter(e) {
    // Створимо масив з дівів даної картки
    const [...divs] = e.target.parentElement.querySelectorAll('div');
    // Створимо об'єкт для збереження даних
    const saveData = {};

    divs.forEach(div => {
        saveData[div.firstElementChild.innerText] = div.lastElementChild.innerText;
    })
    // Внесемо дані у localStorage за ім'ям
    localStorage.setItem(divs[0].lastElementChild.innerText, JSON.stringify(saveData));

    alert('Saved to local storage: ' + divs[0].lastElementChild.innerText);
}

const p = fetch(url, { method: 'GET' })
    .then(response => response.json())
    .then(data => {
        data.results.forEach(ch => {
            const characters = [];
            const character = new Character(ch.name, ch.gender, ch.height,
                ch.skin_color, ch.birth_year, ch.homeworld);

            characters.push(character);
            characters.forEach(ch => ch.getHTML());
        })
    })
    // .finally(() => { 
        
    // })