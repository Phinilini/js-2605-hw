import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './components/app/app'

const element = document.getElementById('root');

const root = ReactDOM.createRoot(element);

root.render(<App></App>);