import React from 'react';
import { months, zodiacs, days } from "../data/data";
import List from "../list/list";

function App() {
    return (
        <>
            <List data={months}/>
            <List data={zodiacs}/>
            <List data={days}/>
        </>
    )
}

export default App;