import React from "react";

function List(props) {
    return (
        <div>
            <ul>
                {props.data.map(el => <li>{el}</li>)}
            </ul>
        </div>
    )
}

export default List;