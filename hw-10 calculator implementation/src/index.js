// * В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
// * При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
// * При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.
// * Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
// * При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно очищать память.

window.addEventListener("DOMContentLoaded", () => {
    const btn = document.querySelector(".keys"),
        display = document.querySelector(".display > input");

    let prev;
    let memSpan = document.createElement("span");
    memSpan.innerText = "m";
    display.parentElement.prepend(memSpan);

    btn.addEventListener("click", function (e) {
        let val = e.target.getAttribute("value");

        // Перевірка яка була натиснута кнопка
        if (/\d/.test(val)) {
            if (calc.sign === "") {
                if (val === "0" && calc.value1 === "")
                    show(0, display);
                else {
                    calc.value1 += val;
                    show(calc.value1, display);
                }
            } else {
                if (val === "0" && calc.value2 === "")
                    show(0, display);
                else {
                    calc.value2 += val;
                    show(calc.value2, display);
                }
            }
        } else if (/\./.test(val)) {
            if (calc.sign === "") {
                if (!/\./.test(calc.value1)) {
                    if (calc.value1 === "" || calc.value1 === "0")
                        calc.value1 += "0" + val;
                    else
                        calc.value1 += val;
                    show(calc.value1, display);
                }
            } else {
                if (!/\./.test(calc.value2)) {
                    if (calc.value2 === "" || calc.value2 === "0")
                        calc.value2 += "0" + val;
                    else
                        calc.value2 += val;
                    show(calc.value2, display);
                }
            }
        } else if (/^[-+*/]$/.test(val)) {
            if (calc.value1 !== "") {
                if (calc.value2 !== "") {
                    calc.value1 = calculate(calc.sign, parseFloat(calc.value1), parseFloat(calc.value2));
                    calc.value2 = "";
                    show(calc.value1, display);
                }
                calc.sign = val;
            } else if (calc.value1 === "" && display.value !== "") {
                calc.value1 = display.value;
                calc.sign = val;
            }
        } else {
            switch (val) {
                case "C":
                    calc.reset();
                    show(calc.value1, display);
                    break;
                case "=":
                    show(calculate(calc.sign, parseFloat(calc.value1), parseFloat(calc.value2)), display); calc.reset();
                    break;
                case "mrc":
                    if (prev === "mrc") {
                        calc.resetMem();
                        memSpan.classList.remove("show");
                        break;
                    }

                    if (calc.sign === "") {
                        calc.value1 = calc.memory;
                        show(calc.value1, display);
                    } else {
                        calc.value2 = calc.memory;
                        show(calc.value2, display);
                    }
                    break;
                case "m+":
                    if (display.value === "")
                        break;

                    if (calc.memory === "")
                        calc.memory = `${parseFloat(display.value)}`;
                    else
                        calc.memory = `${parseFloat(calc.memory) + parseFloat(display.value)}`;
                    memSpan.classList.add("show");
                    // display.classList.add("memory");
                    break;
                case "m-":
                    if (display.value === "")
                        break;

                    if (calc.memory === "")
                        calc.memory = `${parseFloat(display.value)}`;
                    else
                        calc.memory = `${parseFloat(calc.memory) - parseFloat(display.value)}`;
                    memSpan.classList.add("show");
                    break;
            }
        }
        prev = val;
        enableEqSign();
    }, false)


    const calc = {
        value1: "",
        value2: "",
        sign: "",
        memory: "",

        reset() {
            this.value1 = "";
            this.value2 = "";
            this.sign = "";
        },
        resetMem() {
            this.memory = "";
        }
    }

    function enableEqSign() {
        let eq = document.querySelector("input[value='=']");
        if (calc.value1 === "" || calc.value2 === "") {
            eq.setAttribute("disabled", "");
        } else {
            eq.removeAttribute("disabled");
        }
    }

    function show(value, el) {
        el.value = value
    }

    function calculate(sign, a, b) {
        switch (sign) {
            case "+": return sum(a, b);
            case "-": return sub(a, b);
            case "*": return mul(a, b);
            case "/": return div(a, b);
        }
    }

    const sum = (a, b) => a + b;
    const sub = (a, b) => a - b;
    const mul = (a, b) => a * b;
    const div = (a, b) => {
        if (b === 0)
            return;
        else
            return (a / b).toFixed(5);
    }


})