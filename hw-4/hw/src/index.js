// 1.
function map(fn, arr) {
    for (let i = 0; i < arr.length; i++) {
        //arr[i] = fn(arr[i]);
        fn(i, arr);
    }
    return arr;
}

// const double = (x) => {
//     return x * 2;
// }

const double = (i, arr) => {
    arr[i] *= 2;
}

function display(arr, id) {
    let out = ``;
    for (let i = 0; i < arr.length; i++) {
        document.getElementById(id).innerHTML += `${arr[i]}, `;
    }
}

let numbers = [1, 35, -15, 66, 78, 91];
display(numbers, 'before');
map(double, numbers);
display(numbers, 'after');

// 2.
function checkAge1(age) {
    return age > 18 ? true : confirm('Родители разрешили ? 1');
}

function checkAge2(age) {
    return age > 18 || confirm('Родители разрешили ? 2');
}

let age = parseInt(prompt("Please enter your age"));
document.getElementById("checkAge1").innerHTML += `Check age 1: ${checkAge1(age)}`;
document.getElementById("checkAge2").innerHTML += `Check age 2: ${checkAge2(age)}`;