let divs = ``;
for (let i = 0; i < 8; i++) {
    divs += `<div id=task${i + 1}></div>`;
}
document.write(divs);
for (let i = 0; i < 8; i++) {
    targetStyles = {
        width: "200px",
        backgroundColor: "cyan",
        padding: "1rem",
        textAlign: "center",
        border: "2px solid black",
        margin: ".5rem",
        fontSize: "1.25rem",
    }

    let divStyle = document.getElementById(`task${i + 1}`).style;
    for (let prop in targetStyles) {
        divStyle[prop] = targetStyles[prop];
    }
}

const display = (i, data, replace = false) => {
    if (!replace)
        document.getElementById(`task${i}`).innerHTML += data;
    else
        document.getElementById(`task${i}`).innerHTML = data;

}

// 1. Сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день недели на Украинском языке
function fn1(num) {
    switch (num) {
        case 1:
            return "Понеділок";
        case 2:
            return "Вівторок";
        case 3:
            return "Середа";
        case 4:
            return "Четверг";
        case 5:
            return "П'ятниця";
        case 6:
            return "Субота";
        case 7:
            return "Неділя";
        default:
            return "Невірне число";
    }
}
display(1, fn1(1));

// 2. Дана строка вида 'var_text_hello'. Сделайте из него текст 'VarTextHello'.
function fn2(str) {
    let strSplit = str.split("_");
    for (let i = 0; i < strSplit.length; i++) {
        let s = strSplit[i].split("");
        s[0] = s[0].toUpperCase();
        s = s.join("");
        strSplit[i] = s;
    }
    return strSplit.join("");
}
display(2, fn2('var_text_hello'));

// 3. Создайте функцию которая будет заполнять массив 10-ю иксами с помощью цикла.
function fn3(arr, x = "x") {
    for (let i = 0; i < 10; i++) {
        arr.push(x);
    }
    return arr;
}
display(3, fn3([1, 2, 3], "x"));

// 4. Создайте маасив на 50 элементов и заполните каждый элемент его номером, не используя циклы выведите каждый нечетный элемент в параграфе, а четный в диве.
let arr4 = new Array(50);
for (let i = 0; i < arr4.length; i++) {
    arr4[i] = i;
}

arr4.map(x => {
    x % 2 === 1 ? display(4, `<p style="background-color:red">${x}</p>`) :
        display(4, `<div style="background-color:green">${x}</div>`);
});

// 5. Если переменная a больше нуля - то в ggg запишем функцию, которая выводит один !, иначе запишем функцию, которая выводит два !
let a5 = parseFloat(prompt("Task 5: type number a"));
let ggg5;
if (a5 > 0) {
    ggg5 = () => {
        display(5, "!");
    }
}
else {
    ggg5 = () => {
        display(5, "!!");
    }
}
ggg5();

// 6. Используя CallBack function создайте калькулятор который будет от пользователя принимать 2 числа и знак арефметической операции. При вводе не числа или при делении на 0 выводить ошибку.
function calc(a, b, sign) {
    switch (sign) {
        case "+": return add(a, b);
        case "-": return sub(a, b);
        case "*": return mul(a, b);
        case "/": return div(a, b);
    }
}

const add = (a, b) => (a + b);
const sub = (a, b) => (a - b);
const mul = (a, b) => (a * b);
const div = (a, b) => (a / b).toFixed(7);

let expr6 = prompt("Task 6: write simple expression (+, -, *, /)", "2 + 2") ?? "";
expr6 = expr6.split(" ").join(""); // remove all spaces
let a6 = parseFloat(expr6[0]);
let sign6 = expr6[1];
let b6 = parseFloat(expr6[2]);

let res6 = calc(a6, b6, sign6);
display(6, res6);

// 7. Функция ggg принимает 2 параметра: анонимную функцию, которая возвращает 3 и анонимную функцию, которая возвращает 4. Верните результатом функции ggg сумму 3 и 4.
function ggg7(fn1, fn2) {
    return fn1() + fn2();
}

let res7 = ggg7(() => 3, () => 4);
display(7, res7);

// 8. Сделайте функцию, которая считает и выводит количество своих вызовов.
let count = 0;
function fn8() {
    count++;
    display(8, `Визвано: ${count} разів`, true);
}
fn8();
fn8();
fn8();
fn8();
fn8();