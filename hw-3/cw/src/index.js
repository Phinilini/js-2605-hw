// 1. Даны два массива: ['a', 'b', 'c'] и [1, 2, 3]. Объедините их вместе.
let a1 = ['a', 'b', 'c'].concat([1, 2, 3]);

// 2. Дан массив ['a', 'b', 'c']. Добавьте ему в конец элементы 1, 2, 3.
let a2 = ['a', 'b', 'c'];
a2.push(1);
a2.push(2);
a2.push(3);

// 3. Дан массив [1, 2, 3]. Сделайте из него массив [3, 2, 1].
let a3 = [1, 2, 3];
a3.reverse();

// 4. Дан массив [1, 2, 3]. Добавьте ему в конец элементы 4, 5, 6.
let a4 = [1, 2, 3];
a4.splice(a4.length, 0, 4, 5, 6);

// 5. Дан массив [1, 2, 3]. Добавьте ему в начало элементы 4, 5, 6.
let a5 = [1, 2, 3]
a5.unshift(4, 5, 6);

// 6. Дан массив ['js', 'css', 'jq']. Выведите на экран первый элемент.
let a6 = ['js', 'css', 'jq'];
document.write(a6[0] + "<br>");

// 7. Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый элементы [1, 2, 3].
let a7 = [1, 2, 3, 4, 5];
let a7_ = a7.slice(0, 3);

// 8. Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 4, 5].
let a8 = [1, 2, 3, 4, 5];
a8.splice(1, 2);

// 9. Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 2, 10, 3, 4, 5].
let a9 = [1, 2, 3, 4, 5];
a9.splice(2, 0, 10);

// 10. Дан массив [3, 4, 1, 2, 7]. Отсортируйте его.
let a10 = [3, 4, 1, 2, 7];
a10.sort();

// 11. Дан массив с элементами 'Привет, ', 'мир' и '!'. Необходимо вывести на экран фразу 'Привет, мир!'.
let a11 = ['Привет, ', 'мир', '!'];
document.write(a11.join("") + "<br>");

// 12. Дан массив ['Привет, ', 'мир', '!']. Необходимо записать в нулевой элемент этого массива слово 'Пока, ' (то есть вместо слова 'Привет, ' будет 'Пока, ').
let a12 = ['Привет, ', 'мир', '!'];
a12[0] = "Пока, ";

// 13. Создайте массив arr с элементами 1, 2, 3, 4, 5 двумя различными способами.
let arr = [1, 2, 3, 4, 5];
let arr_ = new Array(5);
for (let i = 0; i < arr_.length; i++) {
    arr_[i] = i + 1;
}
let arr___ = "1, 2, 3, 4, 5".split(', ')

// 14. Дан многомерный массив arr:

var arr14 = {
    'ru': ['голубой', 'красный', 'зеленый'],
    'en': ['blue', 'red', 'green'],
};

// Выведите с его помощью слово 'голубой' 'blue' .

document.write(arr14["ru"][0] + " " + arr14["en"][0] + "<br>");

// 15. Создайте массив arr = ['a', 'b', 'c', 'd'] и с его помощью выведите на экран строку 'a+b, c+d'.
let arr15 = ['a', 'b', 'c', 'd'];
document.write("" + arr15[0] + "+" + arr15[1] + ", " + arr15[2] + "+" + arr15[3] + "<br>");

// 16. Запросите у пользователя количество элементов массива. Исходя из данных которые ввел пользователь создайте массив на то количество элементов которое передал пользователь. в кажlом индексе массива храните чило которе будет показывать номер элемента массива.
let elQ = parseInt(prompt("How many elements in array ?"));
let arr16 = new Array(elQ);
for (let i = 0; i < arr16.length; i++) {
    arr16[i] = i;
}

// 17. Сделайте так, чтобы из массива который вы создали выше вывелись все нечетные числа в параграфе, а четные в спане с красным фоном.

for (let i = 0; i < arr16.length; i++) {
    if (arr16[i] % 2 === 0)
        document.write("<span style=\"background-color:red\">" + arr16[i] + "</span>");
    else
        document.write("<p>" + arr16[i] + "</p>");
}
document.write("<br>");

// 18. Напишите код, который преобразовывает и объединяет все элементы массива в одно строковое значение. Элементы массива будут разделены запятой.
var vegetables = ['Капуста', 'Репа', 'Редиска', 'Морковка'];

let str1 = vegetables.join(",");

document.write(str1); // "Капуста, Репа, Редиска, Морковка"