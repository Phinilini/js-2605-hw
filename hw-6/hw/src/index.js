// CW
function Calc() {
    this.read = function() {
        this.a = parseFloat(prompt("Write number a"));
        this.b = parseFloat(prompt("Write number b"));
    }
    this.sum = function() {
        return this.a + this.b;
    }
    this.mul = function() {
        return this.a * this.b;
    }
}
let calc = new Calc();
calc.read();
console.log(`Sum: ${calc.sum()}`);
console.log(`Mul: ${calc.mul()}`);
 
// 1 
const MAX_AGE = 100;
const MAX_HUMANS = 10;

const rnd = x => Math.floor(Math.random() * x + 1);

function Human(age) {
    this.age = age;
}

function mySort(arr, reverse = false) {
    //bubble
    for (let i = 0; i < arr.length; i++) {
        let minmax = arr[i].age;
        let index = i;
        for (let j = i + 1; j < arr.length; j++) {
            if (!reverse ? arr[j].age < minmax : arr[j].age > minmax) {
                minmax = arr[j].age;
                index = j;
            }
        }
        let temp = arr[i];
        arr[i] = arr[index];
        arr[index] = temp;
    }
}
// 10 3 8 9 2 -> 2 3 8 9 10
let humans = [];
for (let i = 0; i < MAX_HUMANS; i++) {
    humans[i] = new Human(rnd(MAX_AGE));
}

mySort(humans);
console.log(humans);
console.log("=============================");
console.log("Reverse");
mySort(humans, true);
console.log(humans);

// humans.sort((a,b) => a.age - b.age);
// console.log(humans);
// console.log("==============");
// console.log("Reverse");
// humans.sort((a,b) => b.age - a.age);
// console.log(humans);

// 2
function Human2(name, sname, age, sex, married) {
    this.name = name;
    this.sname = sname;
    this.age = age;
    this.sex = sex;
    this.married = married;

    let nameTold = 0;

    this.tellName = function () {
        nameTold++;
        return `${this.name} ${this.sname}`;
    }
    this.nameTold = function () {
        return nameTold;
    }
}

Human2.prototype.makeFamily = (a, b) => {
    if (a.married === false && b.married === false) {
        a.married = b;
        b.married = a;
        console.log(`${a.name} ${a.sname} and ${b.name} ${b.sname} created a new family`)
    } else {
        console.log("Can't make a new family. Dispose of the old one.");
    }
}

Human2.prototype.isMarried = function () {
    if (this.married === false)
        return "not married";
    return `married to ${this.married.tellName()}`
}

function generateHuman(married = false) {
    let sex = "",
        name = "",
        sname = "",
        age = 0;
        
    if (Math.floor(Math.random() * 2) === 0) {
        sex = "male";
        name = MALE_NAMES[Math.floor(Math.random() * MALE_NAMES.length)];
    }
    else {
        sex = "female";
        name = FEMALE_NAMES[Math.floor(Math.random() * FEMALE_NAMES.length)];
    }
    sname = SNAMES[Math.floor(Math.random() * SNAMES.length)];
    age = rnd(MAX_AGE);
    return new Human2(name, sname, age, sex, married);
}

let hum1 = generateHuman();
let hum2 = generateHuman();

hum1.makeFamily(hum1, hum2);
console.log(`${hum1.tellName()} is ${hum1.isMarried()}`);
console.log(hum1.tellName());
console.log(hum1.tellName());
console.log(hum1.tellName());
console.log(hum1.tellName());
console.log(`Name told: ${hum1.nameTold()}`);