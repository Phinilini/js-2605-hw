window.onload = () => {
    let inp = document.querySelector("input[type='button']");
    inp.onclick = () => {
        // if (document.querySelectorAll("input").length >= 2)
        //     return;
        // Створити нову кнопку
        let inpDraw = document.createElement("input");
        inpDraw.setAttribute("type","button");
        inpDraw.setAttribute("value","Нарисовать");
        // Замінити стару кнопку новою без onclick
        inp.replaceWith(inpDraw);
        inp = document.querySelector("input[type='button']"); 
        let inpText = document.createElement("input");
        inpText.setAttribute("type","text");
        inpText.setAttribute("placeholder","Диаметр");
        inp.before(inpText);
        
        inp.onclick = clickDrawCircles;
    }

    function clickDrawCircles () {
        if (document.querySelector(".wrap") !== null) {
            let wrap = document.querySelector(".wrap");
            wrap.remove();
        }
        let d = parseFloat(document.querySelector("input[type='text']").value);
        
        let wrap = document.createElement("div");
        wrap.classList.add("wrap");
        let body = document.querySelector("body")
        body.append(wrap);
        for (let i = 0; i < 10; i++) {
            let eli = document.createElement("div");
            wrap.append(eli);
            for (let j = 0; j < 10; j++) {
                let el  = document.createElement("div");
                el.classList.add("circle");
                el.style.width = `${d}px`;
                el.style.height = `${d}px`;
                el.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 70%)`;
                el.onclick = clickOnCircle;
                eli.append(el);
            }
        }
    }

    function clickOnCircle (e) {
        let circle = e.target;
        circle.remove();
    }
}