// Реалізувати функцію підсвічування клавіш

/*  - Кожна кнопка містить назву клавіші на клавіатурі
    - Після натискання вказаних клавіш - та кнопка, на якій написана ця
літера, повина фарбуватись у синій колір. При цьому якщо якась інша
літера вже раніше була пофарбована в синій колір - вона стає чорною.
Наприклад за натисканням Enter перша кнопка забарвлюється в синій колір.
Далі, користувач натискає 'S', і кнопка 'S' забарвлюється в синій колір,
а кнопка 'Enter' знову стає чорною.
 */

const buttons = document.querySelector(".btn-wrapper");
const color = "btn-blue";

const dict = {
    "Enter": "Enter",
    "KeyS": "S",
    "KeyE": "E",
    "KeyO": "O",
    "KeyN": "N",
    "KeyL": "L",
    "KeyZ": "Z",
}

buttons.addEventListener("click", e => {
    if (e.target.nodeName !== "BUTTON")
        return;

    resetColor(color);
    e.target.classList.add(color);
});

window.addEventListener("keydown", e => {
    if (dict.hasOwnProperty(e.code)) {
        let [btn] = [...buttons.children].filter(el => {
            if (dict[e.code] === el.innerText)
                return el;
        })

        resetColor(color);
        btn.classList.add(color);
    }
});

function resetColor(color) {
    [...buttons.children].forEach(el => {
        el.classList.remove(color);
    })
}