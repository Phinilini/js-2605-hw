/*

Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". 
Создать в объекте вложенный объект - "Приложение". 
Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". 
Создать методы для заполнения и отображения документа.

*/

let doc = {
    header: "",
    body: "",
    footer: "",
    date: "",
    appendix: {
        header: {},
        body: {},
        footer: {},
        date: {},
    },
    fill() {
        this.header = prompt("Write header", "header") ?? "header";
        this.body = prompt("Write body", "body") ?? "body";
        this.footer = prompt("Write footer", "footer") ?? "footer";
        this.date = (new Date()).toString();
        if (confirm("Add appendix ?")) {
            this.appendix.header["text"] = prompt("Appendix header text", "appendix") ?? "appendix";
            this.appendix.body["text"] = prompt("Appendix body text", "body") ?? "body";
            this.appendix.footer["text"] = prompt("Appendix header footer", "footer") ?? "footer";
            this.appendix.date["text"] = (new Date()).toString();
        }

    },
    show() {
        let out = "";
        for (let prop in this) {
            if (typeof this[prop] === 'function')
                continue;
            if (typeof this[prop] === 'object') {
                if (this[prop]["header"]["text"] === undefined)
                    continue;
                for (let prop_ in this[prop]) {   
                    out += `<p>${this[prop][prop_]["text"]}<p>`;
                }
            } else {
                out += `<p>${this[prop]}</p>`;
            }
        }
        document.write(out);
    },
}

doc.fill();
doc.show();