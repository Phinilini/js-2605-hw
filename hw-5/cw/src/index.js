function show(i, data, rewrite = false) {
    if (!rewrite)
        document.getElementById(`task${i}`).innerHTML += data;
    else
        document.getElementById(`task${i}`).innerHTML = data;
}

// 1. Используя CallBack function создайте калькулятор который будет от пользователя принимать 2 числа и знак арефметической операции. При вводе не числа или при делении на 0 выводить ошибку.
function calc(a, b, fn) {
    return fn(a, b);
}

const add = (a, b) => {
    return a + b;
}

const sub = (a, b) => {
    return a - b;
}

const mul = (a, b) => {
    return a * b;
}

const div = (a, b) => {
    if (b === 0)
        return;
    return a / b;
}

let s = prompt("Write an expression", "2 + 2") ?? "";
s = s.split(" ").join("").split("");
let a = parseFloat(s[0]), sign = s[1], b = parseFloat(s[2]);

switch (sign) {
    case "+": show(1, calc(a, b, add)); break;
    case "-": show(1, calc(a, b, sub)); break;
    case "*": show(1, calc(a, b, mul)); break;
    case "/": show(1, calc(a, b, div)); break;
}

// 2. Создайте объект заработных плат obj. Выведите на экран зарплату Пети и Коли.

//Этот объект дан:
var obj = { 'Коля': '1000', 'Вася': '500', 'Петя': '200' };
show(2, `Петя: ${obj['Петя']}<br>`);
show(2, `Коля: ${obj['Коля']}<br>`);

/* 3. Создайте объект криптокошилек. В кошельке должно хранится имя владельца, несколько валют Bitcoin, Ethereum, Stellar и в каждой валюте дополнительно есть имя валюты, логотип, несколько монет и курс на сегодняшний день. Также в объекте кошелек есть метод при вызове которого он принимает имя валюты и выводит на страницу информацию. "Добрый день, ... ! На вашем балансе (Название валюты и логотип) осталось N монет, если вы сегодня продадите их то, получите ...грн. */

let wallet = {
    name: "Andrey",
    btc: {
        name: "Bitcoin",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png'>",
        vol: 1,
        price: 23003.94,
    },
    eth: {
        name: "Ethereum",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png'>",
        vol: 2,
        price: 1226.63,
    },
    xlm: {
        name: "Stellar",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png'>",
        vol: 3,
        price: 0.1082,
    },
    out(cName) {
        if (!(cName in this))
            return;
        show(3, `"Добрый день, ${this.name} ! На вашем балансе ${this[cName].name} ${this[cName].logo} осталось ${this[cName].vol} монет, если вы сегодня продадите их то, получите ${(this[cName].price * this[cName].vol * 31).toFixed(2)}грн. */`);
    }
}
wallet.out(prompt("Wallet currency: btc, eth, xlm", "btc"));

// 4. Дана строка вида 'var_text_hello'. Сделайте из него текст 'VarTextHello'.
let s4 = 'var_text_hello';
s4 = s4.split("_");
for (let i = 0; i < s4.length; i++) {
    let temp = s4[i].split("");
    temp[0] = temp[0].toUpperCase();
    temp = temp.join("");
    s4[i] = temp;
}
s4 = s4.join("");
show(4, s4);

//5. Если переменная a больше нуля - то в ggg запишем функцию, которая выводит один !, иначе запишем функцию, которая выводит два !
let ggg5;
function fn5(a) {
    if (a > 0) {
        ggg5 = () => { show(5, "!", true); }
    } else {
        ggg5 = () => { show(5, "!!", true); }
    }
}
fn5(parseFloat(prompt("Task 5. Write a number")));
ggg5();

// 6. Функция ggg принимает 2 параметра: анонимную функцию, которая возвращает 3 и анонимную функцию, которая возвращает 4. Верните результатом функции ggg сумму 3 и 4.
function ggg6(fn1, fn2) {
    return fn1() + fn2();
}
show(6, ggg6(() => 3, () => 4));

// 7. Сделайте функцию, которая считает и выводит количество своих вызовов.
let count7 = 0;
show(7, " " + count7, true);
function fn7() {
    count7++;
    show(7, " " + count7, true);
}

// 8. Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.
function isEmpty(obj = {}) {
    for (let prop in obj) {
        if (prop !== undefined) {
            return false
        }
    }
    return true;
}
let empty8 = {};
let notEmpty8 = { name: "not empty" };
show(8, `Empty: ${isEmpty(empty8)}<br>`);
show(8, `Not Empty: ${isEmpty(notEmpty8)}`);

// 9. Выведите на страницу название валюты цену покупки и цену продажи. https://api.privatbank.ua/p24api/exchange_rates?json&date=11.01.2021
// const url = "https://api.privatbank.ua/p24api/exchange_rates?json&date=11.01.2021";

// async function getCurrency() {   
//     try {
//         const response = await fetch(url, {mode: 'cors'});
//         console.log(response);
//     } catch (error) {
//         console.log(error);
//     }
// }
// getCurrency();

// let xhr = new XMLHttpRequest();

// xhr.open("GET", url, true);
// xhr.send();
// xhr.onload = function() {
//     console.log(`Загружено: ${xhr.status} ${xhr.response}`);
//   };

// 9. Выведите на страницу название валюты цену покупки и цену продажи. https://api.privatbank.ua/p24api/exchange_rates?json&date=11.01.2021
const currency = {
    "date":"11.01.2021",
    "bank":"PB",
    "baseCurrency":980,
    "baseCurrencyLit":"UAH",
    "exchangeRate":[
       {
          "baseCurrency":"UAH",
          "saleRateNB":22.0522000,
          "purchaseRateNB":22.0522000
       },
       {
          "baseCurrency":"UAH",
          "currency":"AZN",
          "saleRateNB":16.6488000,
          "purchaseRateNB":16.6488000
       },
       {
          "baseCurrency":"UAH",
          "currency":"BYN",
          "saleRateNB":11.0280000,
          "purchaseRateNB":11.0280000
       },
       {
          "baseCurrency":"UAH",
          "currency":"CAD",
          "saleRateNB":22.3109000,
          "purchaseRateNB":22.3109000
       },
       {
          "baseCurrency":"UAH",
          "currency":"CHF",
          "saleRateNB":32.2608000,
          "purchaseRateNB":32.2608000,
          "saleRate":32.8500000,
          "purchaseRate":31.0500000
       },
       {
          "baseCurrency":"UAH",
          "currency":"CNY",
          "saleRateNB":4.3822000,
          "purchaseRateNB":4.3822000
       },
       {
          "baseCurrency":"UAH",
          "currency":"CZK",
          "saleRateNB":1.3348000,
          "purchaseRateNB":1.3348000,
          "saleRate":1.3300000,
          "purchaseRate":1.1200000
       },
       {
          "baseCurrency":"UAH",
          "currency":"DKK",
          "saleRateNB":4.6927000,
          "purchaseRateNB":4.6927000
       },
       {
          "baseCurrency":"UAH",
          "currency":"EUR",
          "saleRateNB":34.9090000,
          "purchaseRateNB":34.9090000,
          "saleRate":34.6500000,
          "purchaseRate":34.0500000
       },
       {
          "baseCurrency":"UAH",
          "currency":"GBP",
          "saleRateNB":38.5238000,
          "purchaseRateNB":38.5238000,
          "saleRate":38.9000000,
          "purchaseRate":36.9000000
       },
       {
          "baseCurrency":"UAH",
          "currency":"HUF",
          "saleRateNB":0.0974530,
          "purchaseRateNB":0.0974530
       },
       {
          "baseCurrency":"UAH",
          "currency":"ILS",
          "saleRateNB":8.8862000,
          "purchaseRateNB":8.8862000
       },
       {
          "baseCurrency":"UAH",
          "currency":"JPY",
          "saleRateNB":0.2748400,
          "purchaseRateNB":0.2748400
       },
       {
          "baseCurrency":"UAH",
          "currency":"KZT",
          "saleRateNB":0.0675120,
          "purchaseRateNB":0.0675120
       },
       {
          "baseCurrency":"UAH",
          "currency":"MDL",
          "saleRateNB":1.6444000,
          "purchaseRateNB":1.6444000
       },
       {
          "baseCurrency":"UAH",
          "currency":"NOK",
          "saleRateNB":3.3634000,
          "purchaseRateNB":3.3634000
       },
       {
          "baseCurrency":"UAH",
          "currency":"PLN",
          "saleRateNB":7.7274000,
          "purchaseRateNB":7.7274000,
          "saleRate":7.7000000,
          "purchaseRate":7.2000000
       },
       {
          "baseCurrency":"UAH",
          "currency":"RUB",
          "saleRateNB":0.3840000,
          "purchaseRateNB":0.3840000,
          "saleRate":0.4000000,
          "purchaseRate":0.3600000
       },
       {
          "baseCurrency":"UAH",
          "currency":"SEK",
          "saleRateNB":3.4679000,
          "purchaseRateNB":3.4679000
       },
       {
          "baseCurrency":"UAH",
          "currency":"SGD",
          "saleRateNB":21.4799000,
          "purchaseRateNB":21.4799000
       },
       {
          "baseCurrency":"UAH",
          "currency":"TMT",
          "saleRateNB":8.0785000,
          "purchaseRateNB":8.0785000
       },
       {
          "baseCurrency":"UAH",
          "currency":"TRY",
          "saleRateNB":3.8547000,
          "purchaseRateNB":3.8547000
       },
       {
          "baseCurrency":"UAH",
          "currency":"UAH",
          "saleRateNB":1.0000000,
          "purchaseRateNB":1.0000000
       },
       {
          "baseCurrency":"UAH",
          "currency":"USD",
          "saleRateNB":28.2847000,
          "purchaseRateNB":28.2847000,
          "saleRate":28.4500000,
          "purchaseRate":28.0500000
       },
       {
          "baseCurrency":"UAH",
          "currency":"UZS",
          "saleRateNB":0.0026988,
          "purchaseRateNB":0.0026988
       },
       {
          "baseCurrency":"UAH",
          "currency":"GEL",
          "saleRateNB":8.6366000,
          "purchaseRateNB":8.6366000
       }
    ]
 }
 for (let curr of currency["exchangeRate"]) {
    let name = curr["currency"];
    if (name === undefined)
        continue;
    let buy = curr["saleRateNB"];
    let sell = curr["purchaseRateNB"];
    show(9, `${name} buy: ${buy} sell: ${sell}<br>`);
}