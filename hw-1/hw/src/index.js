let x = 6, y = 14, z = 4;
document.write("0. x = " + x + " , y = " + y + " , z = " + z);
document.write("<br><br>");

//Виконується починаючи з найвищого пріорітету
//1. x++ у цьому прикладі не змінить значення x (16 пріорітет)
//2. * z наступна (13)
//3. y - (12)
//4. x += остання (2)
x += y - x++ * z;
document.write("1. x = " + x + " , y = " + y + " , z = " + z);
document.write("<br>");
x = 6, y = 14, z = 4;
//1. --х (15)
//2. * 5 (13)
//3. - y (12)
//4. z = (2)
z = --x - y * 5;
document.write("2. x = " + x + " , y = " + y + " , z = " + z);
document.write("<br>");
x = 6, y = 14, z = 4;

//1. 5 % z (13)
//2. x + (12)
//3. y /= (2)
y /= x + 5 % z;
document.write("3. x = " + x + " , y = " + y + " , z = " + z);
document.write("<br>");
x = 6, y = 14, z = 4;

//1. x++ (16) Після прикладу x = 7
//2. y * 5 (13)
//3. + (12)
//4. z -= (2)
z -= x++ + y * 5;
document.write("4. x = " + x + " , y = " + y + " , z = " + z);
document.write("<br>");
x = 6, y = 14, z = 4;

//1. x++ (16) Не вплине на х
//2. * z (13)
//3. y - (12)
//4. x = (2)
x = y - x++ * z;
document.write("5. x = " + x + " , y = " + y + " , z = " + z);
document.write("<br>");