const BLOCK = '$';
const SP = '&#160;&#160;';

function wr(arg) {
    document.write(arg);
}

function br() {
    document.write("<br>")
}

// Прямокутник
let wth = parseInt(prompt("width"));
let ht = parseInt(prompt("height"));;
for (let i = 0; i < ht; i++) {
    for (let j = 0; j < wth; j++) {
        wr(BLOCK);
    }
    br();
}
br();

// 2й спосіб 1м циклом
// for (let i = 0; i < wth * ht; i++) {
//     wr(BLOCK);
//     if ((i + 1) % wth === 0) {
//         br();
//     }
// }
// br();

// Прямокутний трикутник

// let counter = 0; j == 0, j++
for (let j = 0; j < wth; j++) { // height = width щоб трикутник завжди малювався повністю
    for (let i = 0 + j; i < wth; i++) {
        wr(BLOCK);
    }
    br();
    //counter++;
}
br();

// for (let i = 0; i < wth; i++) { Від 0 до wth
//     wr(BLOCK);
// }
// br();
 
// for (let i = wth; i > 0; i--) { Від wth до 0
//     wr(BLOCK);
// }
//br();

// Рівнобедренний трикутник

for (let decrese = wth - 1; decrese > 0; decrese--) {
    for (let i = 0; i < (wth - 1) - (decrese - 1); i++) {
        wr(BLOCK);
    }
    br();
}

for (let i = 0; i < wth; i++) {
    wr(BLOCK)
}
br();

for (let j = 0; j < wth - 1; j++) {
    // for (let i = 0; i < wth - j; i++) {
    //     wr(BLOCK);
    // }
    // br();

    let temp = j;
    while (j < wth - 1) {
        wr(BLOCK);
        j++;
    }
    j = temp;

    br();
}
br();

// Рівносторонній трикутник
 
//      $ 
//     $$$               $$
//    $$$$$             $$$$
//   $$$$$$$           $$$$$$
//  $$$$$$$$$         $$$$$$$$
// $$$$$$$$$$$  wth  $$$$$$$$$$

// Math.floor(wth / 2) пробіли
 
let spaces = Math.floor(wth / 2);
for (let omit = 0; omit <= spaces; omit++) { // знак <= тому що коли omit == spaces це значить, що трєба уникнути усих пробілів на останній ітерації
    for (let i = 0; i < spaces - omit; i++) {
        wr(SP);
    }
    for (let i = 0; i < (wth % 2 === 0 ? 0 : 1) + omit * 2; i++) {
        wr(BLOCK);
    }
    br();
}
br();

// Ромб

//      $
//     $$$
//    $$$$$
//   $$$$$$$
//  $$$$$$$$$
// $$$$$$$$$$$  wth
//  $$$$$$$$$
//   $$$$$$$
//    $$$$$
//     $$$
//      $

for (let omit = 0; omit < spaces; omit++) { // знак < тому що основа ромбу окремим циклом
    for (let i = 0; i < spaces - omit; i++) {
        wr(SP);
    }
    for (let i = 0; i < (wth % 2 === 0 ? 0 : 1) + omit * 2; i++) {
        wr(BLOCK);
    }
    br();
}
for (let i = 0; i < wth; i++) {
    wr(BLOCK);
}
br();
for (let decrese = spaces - 1; decrese >= 0; decrese--) {
    for (let i = 0; i < spaces - decrese; i++) {
        wr(SP);
    }
    for (let i = 0; i < (wth % 2 === 0 ? 0 : 1) + decrese * 2 ; i++) {
        wr(BLOCK);
    }
    br();
}