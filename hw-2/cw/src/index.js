// Создайте две целочисленные переменные и присвойте им некоторые значения. По этим значениям, используя вложенные циклы, нарисуйте заполненный прямоугольник из звездочек.

let width = parseInt(prompt("Write a number a"));
let height = parseInt(prompt("Write a number b"));

for (let j = 0; j < height; j++) {
    for (let i = 0; i < width; i++) {
        document.write("*");
    }
    document.write("<br>");
}
document.write("<br>");

// Напишите код, который будет спрашивать логин (prompt). Если посетитель вводит «Админ», то спрашивать пароль, если нажал отмена (escape) – выводить «Вход отменён». Пароль проверять так. Если введён пароль «Властелин», то выводить «Добро пожаловать!», иначе – «Пароль неверен», при отмене – «Вход отменён».
let login = prompt("Write your login", "Админ");


if (login === "Админ") {
    let pass = prompt("Write your password", "Властелин");
    if (pass === null || pass === undefined) {
        alert("Вход отменён");
    } else if (pass === "Властелин") {
        alert("Добро пажаловать !")
    } else {
        alert("Пароль неверен")
    }
} else
    alert("Вход отменён");

