const body = document.querySelector("body");

// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища
class User {
    constructor(name, sname) {
        this.name = name;
        this.sname = sname;
    }

    getFullName(callback = console.log) {
        callback(this.name, this.sname);
    }
}

let user = new User("Ivan", "Ivanov");

user.getFullName();


// Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний
const list = document.createElement("ul");
for (let i = 0; i < 4; i++)
    list.append(document.createElement("li"));


body.append(list);

list.children[0].classList.add("item-blue");
list.children[2].classList.add("item-red");


// Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

const div = document.createElement("div"),
    span = document.createElement("span");

div.style.height = "400px";
div.classList.add("task3")
div.addEventListener("mousemove", e => {
    span.innerText = `X: ${e.clientX} Y: ${e.clientY}`;
});

div.addEventListener("mouseout", e => {
    span.innerText = "";
})

span.style.display = "block";
span.style.height = "1rem";

body.append(div);
body.append(span);


// Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута
const buttons = document.createElement("div");
for (let i = 1; i < 5; i++) {
    let btn = document.createElement("button");
    btn.innerText = i;
    btn.id = `btn-${i}`;
    btn.addEventListener("click", (e) => {
        alert("button №" + /\d/.exec(e.target.id));
    })
    buttons.append(btn);
}
body.append(buttons);

// Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці
const runaway = document.createElement("div");
runaway.classList.add("runaway");
// мінімальна відстань між курсором та елементом
const minDist = 32  , maxMove = 5;

window.addEventListener("mousemove", e => {
    let dist = distance(e.pageX, e.pageY, runaway.offsetLeft, runaway.offsetTop);
    if (dist < minDist) {
        let k = (dist + maxMove) / dist;
        runaway.style.left = e.pageX + (runaway.offsetLeft - e.pageX) * k + "px";
        runaway.style.top = e.pageY + (runaway.offsetTop - e.pageY) * k + "px";
    }
});

function distance (x1, y1, x2, y2) {
    return Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2);
}

body.append(runaway);

// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body
const form = document.createElement("form");
select = document.createElement("select"),
    select_label = document.createElement("label"),
    colorInp = document.createElement("input"),
    colorInp_label = document.createElement("label");

select.id = "select-color";
select.name = "bgcolor";

select_label.setAttribute("for", select.id);
select_label.innerText = "Select color: ";

colorInp.id = "input-color";
colorInp.name = "bgcolor";
colorInp.type = "color";

// Текущий цвет body записываем в value инпута для выбора цвета.
// Т.к. стиль приходит как строка 'rgba(0,0,0,0)', нужно переделать её в hex #000000
window.addEventListener("DOMContentLoaded", () => {
    colorInp.value = (function () {
        let rgb = window.getComputedStyle(body).backgroundColor;
        rgb = rgb.match(/\d+/g);
        // Відрізаємо альфа
        rgb.length = 3;
        rgb = rgb.map(el => {
            el = parseInt(el);
            el = el.toString(16);
            if (el.length < 2)
                el = "0" + el;
            return el;
        })
        rgb = "#" + rgb.join('');
        return rgb;
    })();
});

colorInp_label.setAttribute("for", colorInp.id);
colorInp_label.innerText = "Body";

const colors = {
    "white": "#FFFFFF",
    "red": "#FF0000",
    "green": "#00FF00",
    "blue": "#0000FF",
}

for (let color in colors) {
    let opt = document.createElement("option");
    opt.value = color;
    opt.innerText = color;
    select.append(opt);
}

select.addEventListener("change", e => {
    body.style.backgroundColor = colors[e.target.value];
    colorInp.value = colors[e.target.value];
});

colorInp.addEventListener("input", e => {
    body.style.backgroundColor = e.target.value;
})

form.append(select_label, select, colorInp, colorInp_label);
body.append(form);


// Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль
let login = document.createElement("input");
login.type = "text";
login.placeholder = "Login";

login.addEventListener("input", (e) => {
    console.log("Login: " + e.target.value);
})

body.append(login);


// Створіть поле для введення даних у полі введення даних виведіть текст під полем
let textarea = document.createElement("textarea"),
    textp = document.createElement("p");

textarea.cols = 40;
textarea.rows = 5;

textarea.addEventListener("input", () => {
    textp.innerText = textarea.value;
})

body.append(textarea);
body.append(textp);