const pizza = document.querySelector('div.table'),
    additions = document.querySelector('.ingridients'),
    sauceList = document.querySelector('.result > .sauces'),
    toppingsList = document.querySelector('.result > .topings'),
    price = document.querySelector('.result > .price');

// Розміри піци та яка ціна стоїть по дефолту
const sizes = document.getElementById('pizza');
let basePrice = 250;
let totalPrice = 0;
const SAUCE_PRICE = 10;
const TOPPING_PRICE = 15;

additions.addEventListener('dragstart', e => {
    e.dataTransfer.setData('text/plain', e.target.id);
    e.dataTransfer.effectAllowed = "move";
    if (e.target.classList)
        e.target.classList.add('dragging');
})

additions.addEventListener('dragend', e => {
    if (e.target.classList)
        e.target.classList.remove('dragging');
})


pizza.addEventListener('dragover', drag => {
    drag.preventDefault();
})

pizza.addEventListener('drop', drag => {
    drag.preventDefault();

    const id = drag.dataTransfer.getData('text/plain');
    const idEl = document.getElementById(id);

    const elText = document.createElement('div');
    if (!idEl || !idEl.nextElementSibling)
        return;

    elText.innerText = idEl.nextElementSibling.innerText;

    if (/^(sauce)/.test(id)) {
        sauceList.append(elText);
        changePrice(SAUCE_PRICE);
    } else {
        toppingsList.append(elText);
        changePrice(TOPPING_PRICE);
    }

    const addition = document.createElement('img');
    addition.src = document.getElementById(id).src;
    addition.alt = idEl.nextElementSibling.innerText;

    pizza.append(addition);
})



sizes.addEventListener('change', e => {
    switch (e.target) {
        case sizes.elements[0]: basePrice = 150; break;
        case sizes.elements[1]: basePrice = 200; break;
        case sizes.elements[2]: basePrice = 250; break;
    }
    changePrice(0);
});

function changePrice(add) {
    // Розмір піци + соуси 
    totalPrice += add

    price.children[0].innerText = `Цiна: ${totalPrice + basePrice}`
}


// Видалення додадків при натисканні на назву
function deleteAddition(pr) {
    return function (e) {
        if (e.target.nodeName === 'P' || e.target.children.length)
            return;

        const text = e.target.innerText;

        const additionsOnPizza = [...pizza.children].slice(1);
        additionsOnPizza.findLast(el => el.alt === text).remove();

        e.target.remove();
        changePrice(-pr);
    }
}
sauceList.addEventListener('click', deleteAddition(SAUCE_PRICE))
toppingsList.addEventListener('click', deleteAddition(TOPPING_PRICE));

// Видалення при натисканні на піцу
function deleteFromPizza() {
    const deletables = [...pizza.children].splice(1);
    if (deletables.length > 0) {
        const delAlt = deletables[deletables.length - 1].alt;

        // Якщо соус
        [...sauceList.children].find(el => {
            if (el.innerText === delAlt) {
                el.remove();
                return true;
            }
        });
        // Якщо топінг
        [...toppingsList.children].find(el => {
            if (el.innerText === delAlt) {
                el.remove();
                return true;
            }
        });

        deletables[deletables.length - 1].remove();
    }
}

pizza.addEventListener('click', deleteFromPizza);

// Тікаюча кнопка
const banner = document.getElementById('banner');
banner.onmousemove = e => {
    const bannerW = parseFloat(window.getComputedStyle(banner).width) +
            parseFloat(window.getComputedStyle(banner).paddingLeft) * 2,

        bannerH = parseFloat(window.getComputedStyle(banner).width) +
            parseFloat(window.getComputedStyle(banner).paddingTop) * 2;

    const displayH = visualViewport.height - bannerH,
        displayW = visualViewport.width - bannerW;

    e.target.style.bottom = Math.floor(Math.random() * displayH) + 'px';
    e.target.style.right = Math.floor(Math.random() * displayW) + 'px';
}

window.addEventListener('DOMContentLoaded', () => {
    changePrice(0);
});

// Валідація та відправка форми
const info = document.forms.info;
info.btnSubmit.onclick = () => {
    if (validateOrder(info))
        document.location.href = "thank-you.html";
    else
        alert("Невірно введені дані");
}

function validateOrder(f) {
    // +380505050500
    // +38 050 5050 500
    // email@email.com
    if (
        /^\S+( )?\S+$/.test(f.name.value) &&
        /^\+380\d{9}/.test(f.phone.value) &&
        /^\w+@\w+\.\w{2,}/.test(f.email.value)
    ) return true;
    
    return false;
}